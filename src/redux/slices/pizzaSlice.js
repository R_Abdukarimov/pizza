import axios from 'axios'
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

export const fetchPizzas = createAsyncThunk(
  'pizza/fetchPizzasStatus',
  async (params) => {
    const { category, orderReq, search, sorted, currentPage } = params

    const res = await axios.get(
      `https://63f75f65e8a73b486af676c6.mockapi.io/items?page=${currentPage}&limit=4&
	${category}&sortBy=${sorted}&order=
	${orderReq}${search}`
    )
    return res.data
  }
)

const initialState = {
  items: [],
  status: 'loading', //loading | success | error
}

const pizzaSlice = createSlice({
  name: 'pizza',
  initialState,
  reducers: {
    setItems(state, action) {
      state.items = action.payload
    },
  },
  extraReducers: {
    [fetchPizzas.pending]: (state) => {
      state.status = 'loading'
      state.items = []
    },
    [fetchPizzas.fulfilled]: (state, action) => {
      state.items = action.payload
      state.status = 'success'
    },
    [fetchPizzas.rejected]: (state) => {
      console.log(state, 'rejected')
      state.status = 'error'
      state.items = []
    },
  },
})
export const { setItems } = pizzaSlice.actions

export default pizzaSlice.reducer
