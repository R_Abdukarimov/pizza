import { createSlice } from '@reduxjs/toolkit'

const initialState = {
	searchValue:'',
  categoryId: 0,
  currentPage: 1,
  sort: {
    name: 'популярности',
    sort: 'rating',
  },
  order: true,
}

const filterSlice = createSlice({
  name: 'filters',
  initialState,
  reducers: {
    setCategoryId(state, action) {
      state.categoryId = action.payload
    },
		setSearchValue(state, action) {
      state.searchValue = action.payload
    },
    setSort(state, action) {
      state.sort = action.payload
    },
    setDir(state, action) {
      state.order = action.payload
    },
    setCurrentPage(state, action) {
      state.currentPage = action.payload
    },
  },
})

export const { setCategoryId, setSort, setDir, setCurrentPage ,setSearchValue} =
  filterSlice.actions

export default filterSlice.reducer
