import React, { useEffect } from 'react'

import { useDispatch, useSelector } from 'react-redux'
import { setCategoryId, setCurrentPage } from 'redux/slices/filterSlice'
import { fetchPizzas } from 'redux/slices/pizzaSlice'

import Sort from 'components/Sort'
import Categories from 'components/Categories'
import Skeleton from 'components/PizzaBlock/Skeleton'
import PizzaBlock from 'components/PizzaBlock'
import Pagination from 'components/Pagination'
import Error from 'components/Error'
import { Link } from 'react-router-dom'

const Home: React.FC = () => {
  const dispatch = useDispatch()
  const { categoryId, order, currentPage, searchValue } = useSelector(
    (state: any) => state.filter
  )
  const sort = useSelector((state: any) => state.filter.sort.sort)
  const { status, items } = useSelector((state: any) => state.pizza)

  const onChangeCategory = (id: number) => {
    dispatch(setCategoryId(id))
  }
  const onChangePage = (number: number) => {
    dispatch(setCurrentPage(number))
  }

  const getPizzas = async () => {
    const sorted = sort
    const category = categoryId > 0 ? `category=${categoryId}` : ''
    const orderReq = order ? 'asc' : 'desc'
    const search = searchValue ? `&search=${searchValue}` : ''

    dispatch(
      //@ts-ignore
      fetchPizzas({
        category,
        orderReq,
        search,
        sorted,
        currentPage,
      })
    )

    window.scrollTo(0, 0)
  }

  useEffect(() => {
    getPizzas()
  }, [categoryId, sort, order, searchValue, currentPage])

  const pizzaBlocks = items?.map((pizza: any) => (
    <Link to={`/pizza/${pizza.id}`} key={pizza.id}>
      <PizzaBlock {...pizza} />
    </Link>
  ))
  const skeletons = [...new Array(6)].map((_, index) => (
    <Skeleton key={index} />
  ))
  return (
    <div className='container'>
      <div className='content__top'>
        <Categories value={categoryId} onClickCategory={onChangeCategory} />
        <Sort />
      </div>
      <h2 className='content__title'>Все пиццы</h2>

      {pizzaBlocks?.length === 0 && status === 'success' ? (
        <h1>Пиццы не найдены</h1>
      ) : status === 'error' ? (
        <Error />
      ) : (
        <div className='content__items'>
          {status === 'loading' ? skeletons : pizzaBlocks}
        </div>
      )}

      {pizzaBlocks?.length > 0 && (
        <Pagination currentPage={currentPage} onChangePage={onChangePage} />
      )}
    </div>
  )
}

export default Home
