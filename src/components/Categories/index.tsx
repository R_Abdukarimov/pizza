import React from 'react'

type CategoriesProps = {
  value: number
  onClickCategory: any
}

const Categories: React.FC<CategoriesProps> = ({ value, onClickCategory }) => {
  const categoryItems = [
    'Все',
    'Мясные',
    'Вегетарианская',
    'Гриль',
    'Острые',
    'Закрытые',
  ]

  const liItem = categoryItems.map((item, index) => {
    return (
      <li
        onClick={() => onClickCategory(index)}
        key={index}
        className={value === index ? 'active' : ''}
      >
        {item}
      </li>
    )
  })

  return (
    <div className='categories'>
      <ul>{liItem}</ul>
    </div>
  )
}

export default Categories
